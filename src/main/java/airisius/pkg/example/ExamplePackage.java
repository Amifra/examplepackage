package airisius.pkg.example;

import airisius.core.pkg.AirisiusPackage;
import airisius.core.plugin.PluginsManager;

public class ExamplePackage extends AirisiusPackage {
	@Override
	public void main() {
		PluginsManager.addPlugin(ExamplePlugin.class);
	}
}
