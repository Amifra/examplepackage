package airisius.pkg.example;

import org.bukkit.event.Event;

import com.google.common.collect.Lists;

import airisius.core.command.AirisiusCommand;
import airisius.core.event.AirisiusListener;
import airisius.core.plugin.AirisiusPlugin;

public class ExamplePlugin extends AirisiusPlugin {
	@Override
	public String getName() {
		return "Example";
	}
	
	@Override
	public void onEnable() {
		setListener(new AirisiusListener().listen(Event.class, (event) -> {
			// Handle event
		}));
		addCommand(new AirisiusCommand("example").setExecutor((sender, args) -> {
			sender.sendMessage((String)getConfig("text"));
		}).setTabCompleter((sender, args) -> {
			return Lists.newArrayList();
		}));
		addConfig("text", "Some example text");
	}
}